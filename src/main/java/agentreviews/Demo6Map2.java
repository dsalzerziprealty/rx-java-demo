package agentreviews;

import agentreviews.model.Agent;
import agentreviews.model.AgentReviews;
import agentreviews.service.AgentRecommendationService;
import rx.functions.Func1;

import java.util.List;

/**
 * Created by dsalzer on 5/1/17.
 */
public class Demo6Map2  extends Demo {

    public static void main(String[] args) {
        mlsHomeObervable()
                .filter(mlsHomeFilter())
                .map(mlsHomeToAgentMap())
                .map(new Func1<Agent, List<AgentReviews>>() {
                    @Override
                    public List<AgentReviews> call(Agent agent) {
                        List<AgentReviews> agentReviewss = AgentRecommendationService.getAgentRecommendations(agent.getLicenseNumber());
                        return agentReviewss;
                    }
                })
                .subscribe(agentRecommendationListObserver());
    }
}
