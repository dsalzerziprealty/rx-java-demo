package agentreviews;

import rx.schedulers.Schedulers;

/**
 * Created by dsalzer on 5/1/17.
 */
public class Demo9Scheduler2 extends Demo {

    public static void main(String[] args) {
        mlsHomeObervable()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .filter(mlsHomeFilter())
                .map(mlsHomeToAgentMap())
                .observeOn(Schedulers.io())
                .flatMap(agentToAgentRecommendationFlatMap())
                .subscribe(agentRecommendationObserver());

        waitUntilCompletion();
    }
}
