package agentreviews;

import agentreviews.model.Agent;
import agentreviews.model.AgentReviews;
import agentreviews.model.MLSHome;
import agentreviews.service.AgentRecommendationService;
import agentreviews.service.HomeService;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

import java.util.List;

import java.util.concurrent.Callable;
import java.util.stream.Stream;

/**
 * Created by dsalzer on 4/26/17.
 */
public class Demo {
    private static final boolean PRINT_THREAD = true;

    public static Observable mlsHomeObervable() {
        return Observable.create(new Observable.OnSubscribe<MLSHome>() {
            @Override
            public void call(Subscriber<? super MLSHome> subscriber) {
                try {
                    printThread("mlsHomeObervable");
                    List<MLSHome> mlsHomes = HomeService.getRecentlySoldHomes();
                    for (MLSHome home : mlsHomes) {
                        subscriber.onNext(home);
                    }
                    subscriber.onCompleted();
                } catch (Exception e) {
                    subscriber.onError(e);
                }

            }
        });
    }

    public static Observable<List<MLSHome>> mlsHomeListObservable() {
        Observable obs = Observable.fromCallable(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                return HomeService.getRecentlySoldHomes();
            }
        });
        return obs;
        //return obs.subscribeOn(Schedulers.io()) ; // created an Observable<MLSHome>
    }

    public static Observer<MLSHome> mlsHomeObserver() {
        return new Observer<MLSHome>() {

            public void onNext(MLSHome mlsHome) {
                System.out.println(mlsHome.toString());
            }

            public void onCompleted() {
                System.out.println("We are done!");
            }

            public void onError(Throwable e) {
                System.out.println("We have an error! " + e.getMessage());
            }

        };
    }

    public static Observer<List<MLSHome>> mlsHomeListObserver() {
        return new Observer<List<MLSHome>>() {

            public void onNext(List<MLSHome> mlsHomes) {
                Stream.of(mlsHomes).forEach(System.out::println);
            }

            public void onCompleted() {
                System.out.println("We are done!");
            }

            public void onError(Throwable e) {
                System.out.println("We have an error! " + e.getMessage());
            }

        };
    }

    public static Observer<Agent> agentObserver() {
        return new Observer<Agent>() {

            @Override
            public void onNext(Agent agent) {

                System.out.println(agent.toString());

            }

            public void onCompleted() {
                System.out.println("We are done!");
            }

            public void onError(Throwable e) {
                System.out.println("We have an error! " + e.getMessage());
            }


        };
    }
    public static Observer<List<AgentReviews>> agentRecommendationListObserver() {
        return new Observer<List<AgentReviews>>() {
            @Override
            public void onNext(List<AgentReviews> agentRecList) {

                Stream.of(agentRecList).forEach(System.out::println);

            }

            public void onCompleted() {
                System.out.println("We are done!");
            }

            public void onError(Throwable e) {
                System.out.println("We have an error! " + e.getMessage());
            }


        };
    }
    public static Observer<AgentReviews> agentRecommendationObserver() {
        return new Observer<AgentReviews>() {
            public void onCompleted() {
                printThread("agentRecommendationObserver");
                System.out.println("We are done!");
                IS_COMPLETE = true;
            }

            public void onError(Throwable e) {
                System.out.println("We have an error! " + e.getMessage());
            }

            @Override
            public void onNext(AgentReviews agentRec) {
                System.out.println(agentRec.toString());
            }
        };
    }

    public static Func1<MLSHome, Boolean> mlsHomeFilter() {
        return new Func1<MLSHome, Boolean>() {
            @Override
            public Boolean call(MLSHome mlsHome) {
                printThread("mlsHomeFilter");
                return mlsHome != null && mlsHome.getListingAgentLicenseNumber() != null;
            }
        };
    }

    public static Func1<MLSHome, Agent>mlsHomeToAgentMap() {
        return new Func1<MLSHome, Agent>(){

            @Override
            public Agent call(MLSHome mlsHome) {
                printThread("mlsHomeToAgentMap");
                return mlsHome.getListingAgentObj();
            }
        };
    }

    public static Func1<Agent, Observable<AgentReviews>>agentToAgentRecommendationFlatMap() {
        return new Func1<Agent, Observable<AgentReviews>>() {
            @Override
            public Observable<AgentReviews> call(Agent agent) {
                printThread("agentToAgentRecommendationFlatMap");
                List<AgentReviews> agentReviewss = AgentRecommendationService.getAgentRecommendations(agent.getLicenseNumber());
                return Observable.from(agentReviewss);
            }
        };
    }


    public static void printThread(String name ) {
        if (PRINT_THREAD) {
            System.out.println(">>>>> " + name + ":  " + Thread.currentThread().getName());
        }
    }
    public static boolean IS_COMPLETE = false;
    public static void waitUntilCompletion() {
        while (!IS_COMPLETE) {
            try {
                Thread.currentThread().sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    };
}
