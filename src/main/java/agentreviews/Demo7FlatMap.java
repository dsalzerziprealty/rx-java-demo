package agentreviews;

import agentreviews.model.Agent;
import agentreviews.model.AgentReviews;
import agentreviews.service.AgentRecommendationService;
import rx.Observable;
import rx.functions.Func1;

import java.util.List;

/**
 * Created by dsalzer on 4/27/17.
 */
public class Demo7FlatMap extends Demo {

    public static void main(String[] args) {
        mlsHomeObervable()
                .filter(mlsHomeFilter())
                .map(mlsHomeToAgentMap())
                .flatMap(new Func1<Agent, Observable<AgentReviews>>() {
                    @Override
                    public Observable<AgentReviews> call(Agent agent) {
                        List<AgentReviews> agentReviewss = AgentRecommendationService.getAgentRecommendations(agent.getLicenseNumber());
                        return Observable.from(agentReviewss);
                    }
                })
                .subscribe(agentRecommendationObserver());
    }
}