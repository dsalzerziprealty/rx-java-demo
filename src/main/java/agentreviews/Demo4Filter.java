package agentreviews;

import agentreviews.model.MLSHome;
import rx.functions.Func1;

/**
 * Created by dsalzer on 4/26/17.
 */
public class Demo4Filter extends Demo {

    public static void main(String[] args) {
        mlsHomeObervable()
                .filter(new Func1<MLSHome, Boolean>() {
                    @Override
                    public Boolean call(MLSHome mlsHome) {
                        return mlsHome != null && mlsHome.getListingAgentLicenseNumber() != null;
                    }
                })
                .subscribe(mlsHomeObserver());
    }

}
