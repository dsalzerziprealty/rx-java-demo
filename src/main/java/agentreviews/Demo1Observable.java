package agentreviews;

import agentreviews.model.MLSHome;
import agentreviews.service.HomeService;
import rx.Observable;

import java.util.List;

/**
 * Created by dsalzer on 4/26/17.
 */
public class Demo1Observable extends Demo {

    public static void main(String[] args) {
        List<MLSHome> mlsHomes = HomeService.getRecentlySoldHomes();
        Observable obs = Observable.from(mlsHomes);        // created an Observable<MLSHome>
        obs.subscribe(mlsHomeObserver());
    }
}
