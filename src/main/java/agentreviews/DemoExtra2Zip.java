package agentreviews;

import agentreviews.model.MLSHome;
import rx.Observable;
import rx.functions.Func2;

import java.util.List;

/**
 * Created by dsalzer on 5/3/17.
 */
public class DemoExtra2Zip extends Demo {

    public static void main(String[] args) {
        Observable<List<MLSHome>> obs1 = mlsHomeListObservable();
        Observable<List<MLSHome>> obs2 = mlsHomeListObservable();
        Observable.zip(obs1, obs2, new Func2<List<MLSHome>, List<MLSHome>, List<MLSHome>>() {
            @Override
            public List<MLSHome> call(List<MLSHome> mlsHomes1, List<MLSHome> mlsHomes2) {
                mlsHomes1.addAll(mlsHomes2);
                return mlsHomes1;
            }
        }).subscribe(mlsHomeListObserver());

    }

    public static void yesItSupportsLamdas() {
        Observable<List<MLSHome>> obs1 = mlsHomeListObservable();
        Observable<List<MLSHome>> obs2 = mlsHomeListObservable();
        //
        Observable.zip(obs1, obs2, (h1, h2) -> {
            h1.addAll(h2);
            return h1;
        }).subscribe(mlsHomeListObserver());
    }



}
