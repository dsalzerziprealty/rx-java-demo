package agentreviews;

import agentreviews.model.Agent;
import agentreviews.model.MLSHome;
import rx.functions.Func1;

/**
 * Created by dsalzer on 4/27/17.
 */
public class Demo5Map extends Demo {

    public static void main(String[] args) {
        mlsHomeObervable()
                .filter(mlsHomeFilter())
                .map(new Func1<MLSHome, Agent> (){

                    @Override
                    public Agent call(MLSHome mlsHome) {
                        return mlsHome.getListingAgentObj();
                    }
                })
                .subscribe(agentObserver());
    }
}
