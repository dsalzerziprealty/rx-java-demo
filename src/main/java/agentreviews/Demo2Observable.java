package agentreviews;

import agentreviews.model.MLSHome;
import agentreviews.service.HomeService;
import rx.Observable;

import java.util.List;

/**
 * Created by dsalzer on 4/26/17.
 * Create an Observable with .just
 */
public class Demo2Observable extends Demo {

    public static void main(String[] args) {
        List<MLSHome> mlsHomes = HomeService.getRecentlySoldHomes();
        Observable obs;
        try {
            obs = Observable.just(mlsHomes);        // created an Observable<List<MLSHome>>
        } catch (Exception e) {
            obs = Observable.error(e);
        }
        obs.subscribe(mlsHomeListObserver());
    }
}
