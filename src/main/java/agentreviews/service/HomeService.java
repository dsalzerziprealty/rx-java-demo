package agentreviews.service;

import agentreviews.model.MLSHome;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dsalzer on 4/26/17.
 */
public class HomeService {

    public static  List<MLSHome> mlsHomeList;


    /**
     * returns a list of homes
     * @return
     */
    public static List<MLSHome> getRecentlySoldHomes() {
        if (mlsHomeList == null) init();

        return mlsHomeList;
    }

   public static void init() {
        mlsHomeList = new ArrayList<>();
        mlsHomeList.add(new MLSHome("20244020", "6835 Greeley Hill Rd, Coulterville", "Sabrina Degenhard", null ));
        mlsHomeList.add(new MLSHome("81645679", "1080 Morris Court, Santa Clara", "Juan Guterres", "01977019" ));
        mlsHomeList.add(new MLSHome("201720048", "102 Main Street, Santa Clara", "Jon Hanison", "01102142" ));
   }





}
