package agentreviews.service;

import agentreviews.model.AgentReviews;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dsalzer on 4/27/17.
 */
public class AgentRecommendationService {

    public static Map<String, List<AgentReviews>> recommendationMap;
    public static List<AgentReviews> getAgentRecommendations(String licenceNumber) {
        if (recommendationMap==null) init();
        List<AgentReviews> recommendations = recommendationMap.get(licenceNumber);
        if (recommendations ==null) recommendations = new ArrayList<>();
        return recommendations;
    }

    public static void init() {
        recommendationMap = new HashMap<>();
        List<AgentReviews> l1 = new ArrayList<>();

        l1.add(new AgentReviews("01977019", 4.5F, "Great agent"));
        l1.add(new AgentReviews("01977019", 5.0F, "Juan di a great job"));

        List<AgentReviews> l2 = new ArrayList<>();
        l2.add(new AgentReviews("01102142", 5.0F, "Jon got us a great deal"));
        l2.add(new AgentReviews("01102142", 5.0F, "Awesome Agent"));
        l1.add(new AgentReviews("01102142", 1.0F, "Not a fan"));
        recommendationMap.put("01977019", l1);
        recommendationMap.put("01102142", l2);

    }
}
