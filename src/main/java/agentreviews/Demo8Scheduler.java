package agentreviews;

/**
 * Created by dsalzer on 4/28/17.
 */
public class Demo8Scheduler extends Demo {

    public static void main(String[] args) {
        mlsHomeObervable()

                .filter(mlsHomeFilter())
                .map(mlsHomeToAgentMap())

                .flatMap(agentToAgentRecommendationFlatMap())
                .subscribe(agentRecommendationObserver());

    }
}


/*
    - subscribe on:
      Determines on which thread
 */