package agentreviews;

import agentreviews.model.MLSHome;
import agentreviews.service.HomeService;
import rx.Observable;

import java.util.List;

/**
 * Created by dsalzer on 4/27/17.
 */
public class DemoExtra1Concat extends Demo{
    public static void main(String[] args) {
        List<MLSHome> mlsHomes = HomeService.getRecentlySoldHomes();
        Observable obs = Observable.from(mlsHomes);        // created an Observable<MLSHome>
        Observable obs2 = Observable.from(mlsHomes);
        Observable obsConcat = Observable.concat(obs, obs2);
        obsConcat.subscribe(mlsHomeObserver());
    }
}
