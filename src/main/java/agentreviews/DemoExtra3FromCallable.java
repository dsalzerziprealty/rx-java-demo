package agentreviews;

import agentreviews.service.HomeService;
import rx.Observable;

import java.util.concurrent.Callable;

/**
 * Created by dsalzer on 5/2/17.
 */
public class DemoExtra3FromCallable extends Demo {

    public static void main(String[] args) {
        Observable obs = Observable.fromCallable(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                return HomeService.getRecentlySoldHomes();
            }
        });        // created an Observable<MLSHome>
        obs.subscribe(mlsHomeObserver());
    }
}
