package agentreviews.model;

/**
 * Created by dsalzer on 4/27/17.
 */
public class AgentReviews {
    String agentLicenseNumber;
    public float rating;
    public String comment;

    public AgentReviews(String agentLicenseNumber, float rating, String comment) {
        this.agentLicenseNumber = agentLicenseNumber;
        this.rating = rating;
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "AgentReviews{" +
                "agentLicenseNumber='" + agentLicenseNumber + '\'' +
                ", rating=" + rating +
                ", comment='" + comment + '\'' +
                '}';
    }
}
