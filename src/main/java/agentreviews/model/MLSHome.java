package agentreviews.model;

/**
 * Created by dsalzer on 4/26/17.
 */
public class MLSHome {
    String homeKey;
    String address;
    String listingAgent;
    String listingAgentLicenseNumber;

    public MLSHome(String homeKey, String address, String listingAgent, String listingAgentLicenseNumber) {
        this.homeKey = homeKey;
        this.address = address;
        this.listingAgent = listingAgent;
        this.listingAgentLicenseNumber = listingAgentLicenseNumber;
    }

    public String getHomeKey() {
        return homeKey;
    }

    public void setHomeKey(String homeKey) {
        this.homeKey = homeKey;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getListingAgent() {
        return listingAgent;
    }

    public void setListingAgent(String listingAgent) {
        this.listingAgent = listingAgent;
    }

    public String getListingAgentLicenseNumber() {
        return listingAgentLicenseNumber;
    }

    public void setListingAgentLicenseNumber(String listingAgentLicenseNumber) {
        this.listingAgentLicenseNumber = listingAgentLicenseNumber;
    }

    /**
     * returns the listing agent for this property
     * @return
     */
    public Agent getListingAgentObj() {
        return new Agent(listingAgent, listingAgentLicenseNumber);
    }

    @Override
    public String toString() {
        return "MLSHome{" +
                "homeKey='" + homeKey + '\'' +
                ", address='" + address + '\'' +
                ", listingAgent='" + listingAgent + '\'' +
                ", listingAgentLicenseNumber='" + listingAgentLicenseNumber + '\'' +
                '}';
    }
}
