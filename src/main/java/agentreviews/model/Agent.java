package agentreviews.model;

/**
 * Created by dsalzer on 4/27/17.
 */
public class Agent {
    String name;
    String licenseNumber;

    public Agent(String name, String licenseNumber) {
        this.name = name;
        this.licenseNumber = licenseNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    @Override
    public String toString() {
        return "Agent{" +
                "name='" + name + '\'' +
                ", licenseNumber='" + licenseNumber + '\'' +
                '}';
    }
}
