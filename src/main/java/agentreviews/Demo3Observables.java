package agentreviews;

import agentreviews.model.MLSHome;
import agentreviews.service.HomeService;
import rx.Observable;
import rx.Observer;
import rx.Subscriber;

import java.util.List;

/**
 * Created by dsalzer on 4/26/17.
 */
public class Demo3Observables extends Demo {

    public static void main(String[] args) {

        Observable obs = Observable.create(new Observable.OnSubscribe<MLSHome>() {
            @Override
            public void call(Subscriber<? super MLSHome> subscriber) {
                try {
                    List<MLSHome> mlsHomes = HomeService.getRecentlySoldHomes();
                    for (MLSHome home : mlsHomes) {
                        subscriber.onNext(home);
                    }
                    subscriber.onCompleted();
                } catch (Exception e) {
                    subscriber.onError(e);
                }

            }
        });
        obs.subscribe(new Observer<MLSHome>() {

            public void onNext(MLSHome mlsHome) {
                System.out.println(mlsHome.toString());
            }
            public void onCompleted() {
                System.out.println("We are done!");
            }

            public void onError(Throwable e) {
                System.out.println("We have an error! " + e.getMessage());
            }


        });

    }
}
